module.exports = function(grunt) {

  require('jit-grunt')(grunt, {
    htmllint: 'grunt-html'
  });
  require('time-grunt')(grunt);
  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-mocha-test');
  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-protractor-runner');
  grunt.loadNpmTasks('grunt-bower');
  grunt.loadNpmTasks('grunt-protractor-webdriver');
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    config: grunt.file.readJSON('config.json'),

    watch: {            
      jshint : {
        files: ['src/js/parts/*.es6'],
        tasks: ['babel','concat:js','jshint', 'uglify']
      }
    },

    concat: {
      js: {
        src: [
          'src/js/parts/init-compiled.js',
          'src/js/parts/utilities-compiled.js',
          'src/js/parts/service-compiled.js',
          'src/js/parts/controller-compiled.js',
          'src/js/parts/app-compiled.js',          
        ],
        dest: 'app/js/agl.js'
      }
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish'),
        esversion: 6        
      },
      files: ['Gruntfile.js', 'app/js/parts/*.es6']
    },

    htmllint: {
      all: ['src/*.html']
    },

    babel: {
        options: {
            sourceMap: true,
            presets: ['babel-preset-es2015-without-strict']
        },
        dist: {
            files: [{
                expand: true,
                src: ['src/js/parts/*.es6'],
                ext: '-compiled.js'
            }]
        }
    },

    copy: {
      dev: {
        files: [
          {
            expand: true,
            cwd: 'src/',
            src: ['vendor/**'],
            dest: 'app/'
          },
          {
            expand: true,
            cwd: 'src/',
            src: ['mocks/**'],
            dest: 'app/'
          },
          {
            expand: true,
            cwd: 'src/html',
            src: ['**'],
            dest: 'app/'
          }
        ]
      }
    },

    // To re-instate, add 'uglify' to the build task below, and the watch jshint task above.
    // Also update default.hbs to point to the **min.js version the file.
    uglify: {
      target: {
        files: {
          'app/js/agl.min.js': ['app/js/agl.js']
        }
      }
    },
    browserSync: {
      bsFiles: {
        src : ['src/*.html', 'src/js/*.js']
      },
      options: {
        watchTask: true,
        server: {
          baseDir: 'app'
        },
        ghostMode: {
          clicks: false,
          forms: false,
          scroll: false  
        }
      }
    },
    protractor_webdriver: {
      start: {
          options: {
            path: 'node_modules/protractor/bin',
            command: 'webdriver-manager start',
          },
        },
    },
    protractor: {
      options: {
        keepAlive: false,
        configFile: 'test/protractor.conf.js'
      },
      auto: {
        keepAlive: false,
        options: {
          args: {
            seleniumPort: 4444
          }
        }
      },
      run: {}
    },

    clean: {
      all: ['app','src/js/parts/*compiled*' ]
    },
    karma: {
      config: {
        configFile: 'test/karma.conf.js'
      }
    },
    connect: {
      options: {
        port: 9000,
        hostname: 'localhost'
      },
      test: {
        options: {
          base: ['app']
        }
      }
    }
  });

  grunt.registerTask('default', ['dev']);
  grunt.registerTask('build', ['clean','babel','concat','jshint','uglify', 'copy:dev']);
  grunt.registerTask('dev',['browserSync','watch']);
  grunt.registerTask('unit', ['karma']);
  grunt.registerTask('e2e', ['connect:test','protractor']);
  grunt.registerTask('test', ['build','karma','connect:test','protractor']);

};