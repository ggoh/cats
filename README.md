#Display cat names by owner's gender#

Cats is a single page application that displays a list of cat's names and that is grouped by the gender of the owner.  The cats are retrieved from a [JSON web service](http://agl-developer-test.azurewebsites.net/people.json) hosted by AGL Australia.

Application has been hosted at following endpoint:

* http://ec2-52-62-138-12.ap-southeast-2.compute.amazonaws.com/

## Getting Started
To run the program locally, choose one of the following options to get started:

* Clone the repo: `git clone https://ggoh@bitbucket.org/ggoh/cats.git`
* Download node.js modules: `npm install`
* Install grunt task runner: `npm install -g grunt-cli`
* Update selenium server, `node_modules/protractor/bin/webdriver-manager update`
* Start selenium server, to run protractor test: `node_modules/protractor/bin/webdriver-manager start`
* Run grunt task: `grunt test` (build app and run automated tests)
* Run grunt task: `grunt` (host application)
* Open web browser: http://localhost:3000

## Technology

To develop the application, following tools and technology was used:

*  jQuery (UI event bindings and JSON web service)
*  lodash (Sorting and filtering results)
*  Twitter boostrap based on theme (https://github.com/BlackrockDigital/startbootstrap-freelancer)
*  Jasmine / Karma (Automated Unit testing framework)
*  Protractor (End to end testing framework)
*  Grunt (Task runner to minify, package, test)
*  Node.js (Host the application)

## Issues
Refer to list of outstanding / pending issues for the application.

* https://bitbucket.org/ggoh/cats/issues

## Creator

Cats was created by and is maintained by **[Gabriel Goh].

* https://au.linkedin.com/in/gabgoh

## Special Mentions

Cats has used the boostrap theme created by [Blackrock Digital](http://blackrockdigital,io.) as a boilerplate for the styling and appearance. 

## Copyright and License
Copyright 2016 Gabriel Goh.