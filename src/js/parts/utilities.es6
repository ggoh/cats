let utilities = utilities || {};
(function () {
    /**
     * Utility class to provide filter methods for a json array
     * 
     * @author ggoh
     * 
     * @requires lodash
     * @requires jQuery
     */
    utilities.filter = {

        /**
         * Retrieves all male owners
         */
        retrieveAllMaleOwners: function(json) {
            return _.filter(json, { gender:'Male' });
        },

        /**
        * Retrieves all male owners that have pet cats
        */
        retrieveAllMaleOwnersWithCats: function (json) {            
            return _.filter(json, { gender:'Male', pets:[{ type:'Cat' }] });
        },

        /**
         * Retrieves all female owners
         */
        retrieveAllFemaleOwners: function(json) {
            return _.filter(json, { gender:'Female' });
        },

        /**
        * Retrieves all female owners that have pet cats
        */
        retrieveAllFemaleOwnersWithCats: function (json) {            
            return _.filter(json, { gender:'Female', pets:[{ type:'Cat' }] });
        },

        /**
        * Retrieves all owners that have pet cats
        */
        retrieveAllOwnersWithCats: function (json) {            
            return _.filter(json, { pets:[{ type:'Cat' }] });
        },

        /**
        * Retrieves an array of cat names for all female owners
        */
        retrieveAllCatNamesWithFemaleOwners: function (json) {
            let data = this.retrieveAllFemaleOwnersWithCats(json);
            return this.retrieveAllCatNamesForOwner(data);
        },

        /**
        * Retrieves an array of cat names for all male owners
        */
        retrieveAllCatNamesWithMaleOwners: function (json) {
            let data = this.retrieveAllMaleOwnersWithCats(json);
            return this.retrieveAllCatNamesForOwner(data);
        },

        /**
        * Retrieve an array of cat names for an owner
        */
        retrieveAllCatNamesForOwner: function (json) {
            let totalCats = [];
            $.each(json, function () {
                $.each(this, function(name, value) {
                    if (name === 'pets') {
                        let cats = _.filter(value, { type:'Cat'} );
                        $.each(cats, function () {
                            $.each(this, function(n, v) {
                                if (n === 'name') {
                                    totalCats.push(v);
                                }                                       
                            });
                        });
                    }
                });
            });
            return totalCats;
        },
    };
})($);

(function () {
    /**
     * Utility class to provide sort methods for a json array
     * 
     * @author ggoh
     * 
     * @requires lodash
     */
    utilities.sort = {

        /**
         * Sort an array alphabetically
         * 
         * @param array
         *  
         */
        alphabetically: function(users) {
            return _.sortBy(users);
        },
        randomnly: function(users) {
            return new Promise(function(resolve, reject) {
                    try {
                        if (users.length > 0) {
                            resolve("Users successfully randomnized");                        
                        } else {
                            let error = new Error("No users exist.")
                            reject(error);
                        }                        
                    } catch (err) {
                        reject(error);
                    }
                });
        }
    };
})($);

/**
 * Base Widget Class
*/
class Widget {

    constructor(cssSelector, data) { 
        this.cssSelector = cssSelector;
        this.data = data;
    }

    render() {
        console.log("Rendering of widget via log : " + this.data);
    }
}

/**
 * HTML Widget Class
*/
class HtmlWidget extends Widget {

    constructor(cssSelector, data) {
        super(cssSelector, data);
    }    

    render() {
        super.render();
        let html = '';
        if (this.data.length > 0) {
            for(let i = 0, total=this.data.length; i<total; i++) {
                // Added ES6 notation for template string syntax
                html = `${html}<p class="cat-name text-muted">` + this.data[i] + "</p>";                
            }
            $('#' + this.cssSelector).html(html);
        }
        return html;
    }

    _privateMethod() {
        console.log("Data modifiers are not available in JS, so the underscore is used to identify private methods");
    }
}

/**
 * jQuery Widget Class
*/
class jQueryWidget extends HtmlWidget {

    constructor(cssSelector, data) {
        super(cssSelector, data);
    }

    render() {
        let html = super.render();
        // Render html via jQuery library
        $('#' + this.cssSelector).html(html);
    }
}