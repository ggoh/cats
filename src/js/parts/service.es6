let service = service || {};
(function () {
    /**
     * Service call to retrieve people and their pets
     * 
     * Feature to mock service is available if following command is executed before executing call method:  
     * service.mock(true)
     * 
     * @author ggoh
     *      
     * @requires jQuery
     */
    service = {
        
        url: 'http://agl-developer-test.azurewebsites.net/people.json?callback=?',   

        /**
        * Executes JSON RESTful Service
        * You can fire this 'externally' by calling service.call('controller.callbackSuccess');
        */
        call: function (callback) {
            $.getJSON(this.endpoint(), data => callback(data));
        },
        /**
        * Returns the endpoint of the service
        */
        endpoint: function () {
            return this.url;
        },
        /**
        * Returns the endpoint of the service
        */
        mock: function (setMock=false) {
            this.url = (setMock) ? '/mocks/sample-response.json' : 'http://agl-developer-test.azurewebsites.net/people.json?callback=?';
        }
    };
})($);