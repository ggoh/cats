let controller = controller || {};
(function () {
    /**
     * Controller class that interacts with the service and populates the UI
     * 
     * @author ggoh     
     * 
     * @requires jQuery
     */
    controller = {        

        init: function () {
            this.bindUIActions();
            this.setEndpoint();
        },

        /** 
         * Binds jQuery events to the UI 
        */
        bindUIActions: function () {
            $('.use-stub-btn').click(() => {
                // Added ES6 Parameter Context Mapping
                controller.setEndpoint({isMock:true});
            });
            $('.use-service-btn').click(() => {
                controller.setEndpoint();
            });
        },
        // Added ES6 command to have a default value
        setEndpoint: function(isMock=false) {            
            if (isMock) {
                service.mock(true);
                $('.endpoint-status').html('stub endpoint.');                                
                $('.use-stub-btn').hide();
                $('.use-service-btn').show();
            } else {
                service.mock(false);                
                $('.endpoint-status').html('staging endpoint (AGL Service).');
                $('.use-stub-btn').show();
                $('.use-service-btn').hide();
            }
            $('.endpoint-url').html(service.endpoint());
            service.call(controller.callbackSuccess);
        },

        /**
        * Callback method used for processing a successful response that renders the users on the HTML 
        *
        * @param data - json response from service
        */
        callbackSuccess: function (data) {
            // Retrieve all cats from male and female owners 
            let catsMaleOwners = utilities.filter.retrieveAllCatNamesWithMaleOwners(data);               
            let catsFemaleOwners = utilities.filter.retrieveAllCatNamesWithFemaleOwners(data);

            // Sort list to be in alphabetial order
            catsMaleOwners = utilities.sort.alphabetically(catsMaleOwners);
            catsFemaleOwners = utilities.sort.alphabetically(catsFemaleOwners);
            
            // Render data onto page via html widget class
            new jQueryWidget('maleList', catsMaleOwners).render();            

            // Render data onto page via object method
            controller.renderContent('femaleList', catsFemaleOwners);

            // Add both male and female cats
            let totalCats = catsMaleOwners;
            for (let i=0, total=catsFemaleOwners.length; i<total;i++) {
                totalCats.push(catsFemaleOwners[i]);
            }
            return totalCats;
        },

        /**
         * Renders the HTML to display the name in a list
         * 
         * @param selectorId - HTML element identifier that will be injected
         * @param data - json array that will be populated
         */
        renderContent: function(selectorId, data) {
            let html = '';
            if (data.length > 0) {
                for(let i = 0, total=data.length; i<total; i++) {
                    // Added ES6 notation for template string syntax
                    html = `${html}<p class="cat-name text-muted">${data[i]}</p>`;                
                }
                $('#' + selectorId).html(html);
            }
            return html;           
        }
    };
})($);