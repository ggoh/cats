exports.config = {
  frameworks: ['jasmine'],
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['e2e/specs/catsSpec.js']
};