describe('List of cats names grouped by their owner gender', function () {

	it('Given json web service has occurred, should display a cat name from a male', function () {

		// Allow usage on non-angular apps
		browser.ignoreSynchronization = true;
		browser.driver.get('http://localhost:9000');

		// Validate cat name exists for male gender    
		var styleClass = element(by.id('maleList')).element(by.css('div > p')).getAttribute('class');
		expect(styleClass).toEqual('cat-name text-muted');
	});

	it('Given json web service has occurred, should display a cat name from a female', function () {

		// Allow usage on non-angular apps
		browser.ignoreSynchronization = true;
		browser.driver.get('http://localhost:9000');

		// Validate first li on html is 'grunt
		var styleClass = element(by.id('femaleList')).element(by.css('div > p')).getAttribute('class');
		expect(styleClass).toEqual('cat-name text-muted');
	});

	it('Given use stub button is clicked, should display Madame as first female value (via stub)', function () {

		// Allow usage on non-angular apps
		browser.ignoreSynchronization = true;
		browser.driver.get('http://localhost:9000');

		// Trigger stub
		element(by.id('stubButton')).click();

		// Verfiy stub values of first female
		var firstUser = element(by.id('femaleList')).element(by.css('div > p')).getText();
		expect(firstUser).toEqual('Madame');
	});

	it('Given use stub button is clicked, should display Jimbo as first male value (via stub)', function () {

		// Allow usage on non-angular apps
		browser.ignoreSynchronization = true;
		browser.driver.get('http://localhost:9000');

		// Trigger stub
		element(by.id('stubButton')).click();

		// Verfiy stub values of first male
		var firstUser = element(by.id('maleList')).element(by.css('div > p')).getText();
		expect(firstUser).toEqual('Jimbo');
	});

	it('Given application is successfully hosted, should display title of page', function () {

		// Allow usage on non-angular apps
		browser.ignoreSynchronization = true;

		// Begin
		browser.driver.get('http://localhost:9000');
		expect(element(by.id('intro-heading')).getAttribute('class')).toEqual('intro-heading');
	});

});