describe("Controller Service - Filters json response and populates DOM.", function() {
   
  it ("Given callbacksuccess method, should return correct number of female and male cats", function() {    
    var owners = [
      { name: 'steve', gender:'Male', age:45, pets: [{name: 'Mysteria', type:'Cat'},{ name: 'Sparkles', type: 'Cat' }] },      
      { name: 'fred', gender:'Male',age: 40, pets: [{name: 'Mighty Mouse', type:'Mouse'}] },
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] },
      { name: 'jenny', gender:'Female', age: 37, pets:[{name:'Odie', type:'Dog'}]},
      { name: 'sally', gender:'Female', age: 22, pets:[{name:'Lyla', type:'Cat'}]}
    ];
    var expected = [
      { name: 'Garfield', type: 'Cat'},
      { name: 'Lyla', type:'Cat'},
      { name: 'Mysteria', type:'Cat'},
      { name: 'Sparkles', type: 'Cat'}
    ];
    service.mock(true);        
    controller.init();    
    controller.setEndpoint({isMock:true});
    var actual = controller.callbackSuccess(owners);
    expect(expected.length).toEqual(actual.length);
  })

});