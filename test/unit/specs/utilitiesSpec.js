describe("Utilities Test Modules ", function () {

  it("Given list contains three owners, when filter applied (men only), should contain two in list.", function() {   
    var owners = [
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] },
      { name: 'fred', gender:'Male',age: 40, pets: [{name: 'MightyMouse', type:'Mouse'}] },
      { name: 'jenny', gender:'Female', age: 37, pets:[{name:'Odie', type:'Dog'}]}
    ]; 
    var expected = [
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] },
      { name: 'fred', gender:'Male',age: 40, pets: [{name: 'MightyMouse', type:'Mouse'}]}
    ];
    var actual = utilities.filter.retrieveAllMaleOwners(owners);
    expect(actual).toEqual(expected);
  })

  it("Given list contains four owners, when filter applied (cats and men only) should contain one in list.", function () {
    var owners = [
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] },
      { name: 'Steve', gender:'Male', age:45, pets:null},
      { name: 'fred', gender:'Male',age: 40, pets: [{name: 'MightyMouse', type:'Mouse'}] },
      { name: 'jenny', gender:'Female', age: 37, pets:[{name:'Odie', type:'Dog'}]}
    ];
    var expected = [
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] }
    ];
    var actual = utilities.filter.retrieveAllMaleOwnersWithCats(owners);
    expect(expected).toEqual(actual);
  })

  it("Given list contains five owners, when filter applied (female only) should contain two in list.", function() {
    var owners = [
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] },
      { name: 'Steve', gender:'Male', age:45, pets:null},
      { name: 'fred', gender:'Male',age: 40, pets: [{name: 'Mighty Mouse', type:'Mouse'}]},
      { name: 'jessica', gender:'Female', age: 22, pets: [{ name: 'Pinky', type: 'Cat' }] },
      { name: 'jenny', gender:'Female', age: 37, pets:[{name:'Odie', type:'Dog'}]}
    ];
    var expected = [
      { name: 'jessica', gender:'Female', age: 22, pets: [{ name: 'Pinky', type: 'Cat' }] },
      { name: 'jenny', gender:'Female', age: 37, pets:[{name:'Odie', type:'Dog'}]}
    ];
    var actual = utilities.filter.retrieveAllFemaleOwners(owners);
    expect(expected).toEqual(actual);
  })

  it("Given list contains five owners, when filter applied (cats and female only) should contain one in list.", function() {
    var owners = [
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] },
      { name: 'Steve', gender:'Male', age:45, pets:null},
      { name: 'fred', gender:'Male',age: 40, pets: [{name: 'Mighty Mouse', type:'Mouse'}]},
      { name: 'jessica', gender:'Female', age: 22, pets: [{ name: 'Pinky', type: 'Cat' }] },
      { name: 'jenny', gender:'Female', age: 37, pets:[{name:'Odie', type:'Dog'}]}];
    var expected = [
      { name: 'jessica', gender:'Female', age: 22, pets: [{ name: 'Pinky', type: 'Cat' }] },
    ];
    var actual = utilities.filter.retrieveAllFemaleOwnersWithCats(owners);
    expect(expected).toEqual(actual);
  })  

  it("Given list contains five owners, when filter applied (owners that have cats), should contain two in list.", function() { 
     var owners = [
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] },
      { name: 'steve', gender:'Male', age:45, pets:null},
      { name: 'fred', gender:'Male',age: 40, pets: [{name: 'Mighty Mouse', type:'Mouse'}]},
      { name: 'jessica', gender:'Female', age: 22, pets: [{ name: 'Pinky', type: 'Cat' }] },
      { name: 'jenny', gender:'Female', age: 37, pets:[{name:'Odie', type:'Dog'}]}];
    var expected = [
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] },
      { name: 'jessica', gender:'Female', age: 22, pets: [{ name: 'Pinky', type: 'Cat' }] },
    ];
    var actual = utilities.filter.retrieveAllOwnersWithCats(owners);
    expect(expected).toEqual(actual);
  })

   it("Given list contains six owners, when filter applied (female owners that have cats), should contain three in list.", function() { 
     var owners = [
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] },
      { name: 'steve', gender:'Male', age:45, pets:null},
      { name: 'fred', gender:'Male',age: 40, pets: [{name: 'Mighty Mouse', type:'Mouse'}]},
      { name: 'jessica', gender:'Female', age: 22, pets: [{ name: 'Pinky', type: 'Cat' },{ name: 'Mikka', type: 'Cat' }] },
      { name: 'jenny', gender:'Female', age: 37, pets:[{name:'Odie', type:'Dog'}]},
      { name: 'sophie', gender:'Female', age: 22, pets:[{name:'Madame', type:'Cat'}]}];
    var expected = ['Pinky', 'Mikka', 'Madame'];
    var actual = utilities.filter.retrieveAllCatNamesWithFemaleOwners(owners);
    expect(expected).toEqual(actual);
  })

  it("Given list contains six owners, when filter applied (male owners that have cats), should contain three in list.", function() { 
     var owners = [
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] },
      { name: 'steve', gender:'Male', age:45, pets:null},
      { name: 'fred', gender:'Male',age: 40, pets: [{name: 'Mighty Mouse', type:'Mouse'}]},
      { name: 'jessica', gender:'Female', age: 22, pets: [{ name: 'Pinky', type: 'Cat' },{ name: 'Mikka', type: 'Cat' }] },
      { name: 'jenny', gender:'Female', age: 37, pets:[{name:'Odie', type:'Dog'}]},
      { name: 'chris', gender:'Male', age: 22, pets:[{name:'Madame', type:'Cat'}, {name:'Sparkles', type:'Cat'}]}];
    var expected = ['Garfield','Madame','Sparkles'];
    var actual = utilities.filter.retrieveAllCatNamesWithMaleOwners(owners);
    expect(expected).toEqual(actual);
  })

  it("Given list contains six owners, when filter applied (owners that have cats), should contain six in list.", function() { 
     var owners = [
      { name: 'barney', gender:'Male', age: 36, pets: [{ name: 'Garfield', type: 'Cat' }] },
      { name: 'steve', gender:'Male', age:45, pets:null},
      { name: 'fred', gender:'Male',age: 40, pets: [{name: 'Mighty Mouse', type:'Mouse'}]},
      { name: 'jessica', gender:'Female', age: 22, pets: [{ name: 'Pinky', type: 'Cat' },{ name: 'Mikka', type: 'Cat' }] },
      { name: 'jenny', gender:'Female', age: 37, pets:[{name:'Odie', type:'Dog'}]},
      { name: 'chris', gender:'Male', age: 22, pets:[{name:'Madame', type:'Cat'}, {name:'Sparkles', type:'Cat'}]}];
    var expected = ['Garfield','Pinky','Mikka','Madame','Sparkles'];
    var actual = utilities.filter.retrieveAllCatNamesForOwner(owners);
    expect(expected).toEqual(actual);
  })

  it("Given array of four names, when sort applied, should order alphabetically.", function () {
    var names = ['steve','fred','barney','jenny'];
    var expected = ['barney','fred','jenny','steve'];
    var actual = utilities.sort.alphabetically(names);
    expect(expected).toEqual(actual);
  })

  it("Given array of four names, resolve promise should be returned", function () {
    var names = ['steve','fred','barney','jenny'];
    var expected = "Users successfully randomnized";
    var isSuccess;
    var actual = '';
    utilities.sort.randomnly(names)
    .then(function (log) {
      actual = log;
      isSuccess = true;
      expect(true).toEqual(isSuccess);
    })
    .catch(function (log) {
      // fail test if reject promise returned
      expect(true).toEqual(false);
    });    

  })  

  it("Given empty array, reject promise should be returned", function () {
    var names = [];
    var expected = "Users successfully randomnized";
    var isSuccess;
    utilities.sort.randomnly(names)
    .then(function (log) {
      isSuccess = true;
      // fail test if resolve promise returned
      expect(true).toEqual(false);
    })
    .catch(function (log) {
      isSuccess = false;
      expect(fail).toEqual(isSuccess);
    });    
  })    

  it("Given null value passed, reject promise should be returned", function () {
    var names = [];
    var expected = "Users successfully randomnized";
    var isSuccess;
    utilities.sort.randomnly(null)
    .then(function (log) {
      isSuccess = true;
      // fail test if resolve promise returned
      expect(true).toEqual(false);
    })
    .catch(function (log) {
      isSuccess = false;
      expect(fail).toEqual(isSuccess);
    });    
  })   

});