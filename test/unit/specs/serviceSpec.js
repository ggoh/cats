describe("RESTFul Service - Retrieves list of people from JSON service", function() {

  it ("Given the service is executed, json response should return at least one entry.", function() {    
      service.mock(true);
      service.call(function (data) {
        expect(data.length).toBeGreaterThan(0);
      });      
  })

  it ("Given the service is executed, json response should return greater than one object.", function() {    
      service.mock(true);
      service.call(function (data) {
        expect(data.length).toBeGreaterThan(0);
      });      
  })

});