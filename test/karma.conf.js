module.exports = function(config) {
  config.set({
    basePath: '../',
    frameworks: ['jasmine'],
    browsers:  ['Chrome'],
    files: [
      // Libraries via cdn
      'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js',
      'https://cdn.jsdelivr.net/lodash/4.15.0/lodash.min.js',

      // JS files to test      
      { pattern:'app/js/agl.js', included: true, served: true, watched: true},

      // JS specs
      { pattern:'test/unit/specs/*Spec.js', included: true, served: true, watched: true}

    ],
    reporters: ['progress', 'html', 'coverage', 'threshold' ],

    htmlReporter: {
      outputDir: "test/target/report/results"
    },

    coverageReporter: {
      type: "html",
      dir: "test/target/report/coverage",
      watermarks: {
        statements: [ 70, 80 ],
        functions: [ 70, 80 ],
        branches: [ 70, 80 ],
        lines: [ 70, 80 ]
      }
    },
    thresholdReporter: {
      statements: 80,
      branches: 75,
      functions: 80,
      lines: 80
    },
    preprocessors: {'app/js/*.js': ['coverage'] },
    background: false,
    singleRun: true
  });
};